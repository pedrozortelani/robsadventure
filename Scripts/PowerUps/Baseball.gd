extends KinematicBody2D

var bounces = 0
var dir = 0
var done = false
var xVel = 12
var yVel = -10
var velocity = Vector2(0,0)
var rob

func _ready():
	velocity.x = xVel * dir 
	var t = Timer.new()
	t.connect("timeout", self, "call_out")
	t.set_wait_time(3.5)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	
func _physics_process(delta):
	if !done:
		velocity.y = velocity.y + 40 * delta
		
		var collision = move_and_collide(velocity)
	
		if collision:
			if !collision.collider.has_method("take_damage"):
				bounces += 1
				velocity.x -= 2 * dir
				dir *= -1
				if bounces == 4:
					done = true
					$AnimatedSprite.play("out")
				$Bounce.play()
				velocity.y = 14
				velocity = velocity.bounce(collision.normal)
			elif collision.collider.has_method("take_damage"):
				collision.collider.take_damage(1)
				done = true
				$AnimatedSprite.play("out")

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "out":
		rob.powerup_count -= 1
		queue_free()

func call_out():
	$AnimatedSprite.play("out")
