extends Node2D

var rob
var active = false

func _ready():
	if get_parent().get_name() == "Rob":
		rob = get_parent()
		set_position(Vector2(0, -10))

func _process(delta):
	if Input.is_action_just_pressed("action"):
		active = true
		
		if rob.velocity.y > 140:
			rob.velocity.y = 140
		self.show()
	elif Input.is_action_just_released("action"):
		rob.fall_force = 2000
		active = false
		$Gliding.stop()
		self.hide()
		
	if active:
		$AnimatedSprite.flip_h = rob.get_node("AnimatedSprite").flip_h
		
		if rob.velocity.y > 0:
			rob.fall_force = 70
			if !$Gliding.playing:
				$Gliding.play()
		else:
			rob.fall_force = 2000
			$Gliding.stop()

func get_class():
	return "powerup"
