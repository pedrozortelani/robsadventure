extends Area2D

var powerup = ""
var instantiable = false
var powerup_node
var child_instantiated
var done = false
var taken = false

func _ready():
	powerup_node = load("res://Scenes/PowerUps/" + powerup + ".tscn")

func _on_PowerUp_body_entered(body):
	if body.get_name() == "Rob" and !taken:
		taken = true
		$Pickup.play()
		body.health = 3
		
		if body.powerup == "PalmLeaf":
			body.fall_force = 2000
			child_instantiated = true
		
		if child_instantiated:
			for n in body.get_children():
				if n.has_method("get_class"):
					if n.get_class() == "powerup":
						body.remove_child(n)
						n.queue_free()

		body.powerup = powerup
		if instantiable:
			var new_powerup = powerup_node.instance()
			body.add_child(new_powerup)
			new_powerup.name = powerup

		hide()
		body.HUD.adjustHUD(body)
		done = true

func _on_Pickup_finished():
	if done:
		queue_free()
