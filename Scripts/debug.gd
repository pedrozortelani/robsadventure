extends Node2D

var timer = null

func _ready():
	timer = Timer.new()
	add_child(timer)
	timer.connect("timeout", self, "on_Timer_Timeout")
	timer.set_wait_time(1)
	timer.set_one_shot(false)
	timer.start()

func on_Timer_Timeout():
	print("FPS: " + str(Engine.get_frames_per_second()))
