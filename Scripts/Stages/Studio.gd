extends Node2D

var scene

func _ready():
	scene = load("res://Scenes/TitleScreen.tscn")
	var t = Timer.new()
	t.set_wait_time(5)
	t.set_one_shot(true)
	self.add_child(t)
	t.start()
	yield(t,"timeout")
	end_cutscene()

func end_cutscene():
	var scene_instance = scene.instance()
	var fader = get_parent().get_node("SceneTranstition/Fader")
	var tween = get_parent().get_node("SceneTranstition/Tween")
	tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 1, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	get_parent().add_child(scene_instance)
	get_parent().remove_child(self)
	tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	queue_free()
