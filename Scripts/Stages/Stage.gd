extends Node2D

export var world = ""
export var world_index = 0
export var number = 0
var checkpoint_position
var lobby

func _ready():
	lobby = load("res://Scenes/Stages/" + world + "/Lobby.tscn")
	checkpoint_position = get_node("CheckPoint").global_position

func end_stage():
	var new_lobby = lobby.instance()
	new_lobby.instantiate(get_node("Rob"))
	var fader = get_parent().get_node("SceneTranstition/Fader")
	var tween = get_parent().get_node("SceneTranstition/Tween")
	tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 1, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	get_parent().add_child(new_lobby)
	get_parent().remove_child(self)
	tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	queue_free()
