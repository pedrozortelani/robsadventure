extends Node2D

enum SCREEN{
	Title,
	Continue,
	Controls,
	Slots
}

var slots
var buttons
var controls
var cur_screen = SCREEN.Title
var index = 0
var hl_box = preload("res://Assets/HUD/hlouterbox.png")
var no_box = preload("res://Assets/HUD/outbox.png")
const new_game = preload("res://Scenes/CutScenes/CSNewGame.tscn")

func _ready():
	slots = get_node("Title/Slots")
	buttons = get_node("Title/Buttons")
	controls = get_node("Controls")

func _process(delta):
	match cur_screen:
		SCREEN.Title:
			if Input.is_action_just_pressed("jump"):
				if index == 0:
					cur_screen = SCREEN.Slots
					buttons.hide()
					slots.show()
					index = 1
				elif index == 1:
					get_node("Continue").show()
					cur_screen = SCREEN.Continue
				elif index == 2:
					controls.show()
					cur_screen = SCREEN.Controls
			elif Input.is_action_just_pressed("up") and index > 0:
				index -= 1
				buttons.get_node("Selector").position.y -= 70 
			elif Input.is_action_just_pressed("down") and index < 2:
				index += 1
				buttons.get_node("Selector").position.y += 70 
		SCREEN.Continue:
			pass
		SCREEN.Controls:
			if Input.is_action_just_pressed("action"):
				index = 0
				cur_screen = SCREEN.Title
				buttons.get_node("Selector").position = Vector2(460, 318)
				controls.hide()
		SCREEN.Slots:
			if Input.is_action_just_pressed("jump"):
				start_game(null)
			elif Input.is_action_just_pressed("left") and index > 1:
				get_node("Title/Slots/Slot" + str(index)).get_node("Outbox").set_texture(no_box)
				index -= 1
				get_node("Title/Slots/Slot" + str(index)).get_node("Outbox").set_texture(hl_box)
			elif Input.is_action_just_pressed("right") and index < 3:
				get_node("Title/Slots/Slot" + str(index)).get_node("Outbox").set_texture(no_box)
				index += 1
				get_node("Title/Slots/Slot" + str(index)).get_node("Outbox").set_texture(hl_box)
			elif Input.is_action_just_pressed("action"):
				index = 0
				cur_screen = SCREEN.Title
				slots.hide()
				buttons.show()

func start_game(index):
	if index:
		print("LOAD GAME")
	else:
		var new_game_instance = new_game.instance()
		var fader = get_parent().get_node("SceneTranstition/Fader")
		var tween = get_parent().get_node("SceneTranstition/Tween")
		tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 1, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tween.start()
		yield(tween, "tween_completed")
		get_parent().add_child(new_game_instance)
		get_parent().remove_child(self)
		tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN)
		tween.start()
		yield(tween, "tween_completed")
		queue_free()
