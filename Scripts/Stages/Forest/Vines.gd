extends Area2D

func _on_Vines_body_entered(body):
	if body.get_name() == "Rob":
		body.onVine += 1
		body.can_jump = true
		if body.velocity.y > 0:
			if !body.get_node("Sound/OnVine").playing:
				body.get_node("Sound/OnVine").play()
			body.get_node("AnimatedSprite").play("WallSlide")
			body.fall_force = 0
			body.velocity.y = 150

func _on_Vines_body_exited(body):
	if body.get_name() == "Rob":
		body.onVine -= 1
		if body.onVine == 0:
			body.get_node("Sound/OnVine").stop()
			body.get_node("AnimatedSprite").play("Fall")
			body.can_jump = false
			body.fall_force = 2000
