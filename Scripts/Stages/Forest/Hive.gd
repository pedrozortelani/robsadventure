extends Node2D

var can_spawn = false
var summoning = false
var wasp_count = 0
var wasp = null
var wasp_scene = preload("res://Scenes/Stages/Forest/Wasp.tscn")

func _process(delta):
	if wasp_count < 3 and !summoning and can_spawn:
		summoning = true
		
		var t1 = Timer.new()
		t1.set_wait_time(wasp_count)
		t1.set_one_shot(true)
		self.add_child(t1)
		t1.start()
		yield(t1,"timeout")
		
		$AnimatedSprite.play("summon")
		
		var t2 = Timer.new()
		t2.set_wait_time(2)
		t2.set_one_shot(true)
		self.add_child(t2)
		t2.start()
		yield(t2,"timeout")
		
		wasp = wasp_scene.instance()
		add_child(wasp)
		if wasp_count == 0:
			wasp.position.x -= 25
			wasp.position.y += 5
		elif wasp_count == 1:
			wasp.position.y += 25
		else:
			wasp.position.x += 25
			wasp.position.y += 5
		
		$AnimatedSprite.play("idle")
		wasp_count += 1
		summoning = false


func _on_SpawnTrigger_body_entered(body):
	if body.get_name() == "Rob":
		can_spawn = true


func _on_SpawnTrigger_body_exited(body):
	if body.get_name() == "Rob":
		can_spawn = false
