extends Node2D

export var text = []
export var writing = false
export var next_scene = ""
var rob = load("res://Scenes/Rob.tscn")
var scene
var index = 0
var done = false
export var end_type = ""

func _ready():
	scene = load("res://Scenes/Stages/" + next_scene)

func _process(_delta):
	if not done:
		if not writing and text.size() > index:
			if Input.is_action_just_pressed("jump") or index == 0:
				writing = true
				$Label.text += text[index] + "\n"
				writing = false
				if index > 4:
					$Label.lines_skipped += 1
				index += 1
	
		if end_type == "text" and index >= text.size():
			done = true
			var t = Timer.new()
			t.set_wait_time(5)
			t.set_one_shot(true)
			self.add_child(t)
			t.start()
			yield(t,"timeout")
			end_cutscene()


func _on_AnimatedSprite_animation_finished():
	if end_type == "animation":
		done = true
		end_cutscene()

func end_cutscene():
	var scene_instance = scene.instance()
	var fader = get_parent().get_node("SceneTranstition/Fader")
	var tween = get_parent().get_node("SceneTranstition/Tween")
	tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 1, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")
	get_parent().add_child(scene_instance)
	get_parent().remove_child(self)
	tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	queue_free()
