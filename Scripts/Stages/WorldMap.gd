extends Node2D

var index = 0
var active = false
var lobby = null
onready var rob = get_node("Rob")

func _process(delta):
	if Input.is_action_just_pressed("right") and index < 1 and !active:
		remove_child(rob)
		rob.set_offset(0)
		get_node("Path0To1").add_child(rob)
		active = true
		index += 1
		rob.get_node("AnimatedSprite").flip_h = false
	elif Input.is_action_just_pressed("left") and index > 0 and !active:
		remove_child(rob)
		rob.set_offset(0)
		get_node("Path1To0").add_child(rob)
		active = true
		index -= 1
		rob.get_node("AnimatedSprite").flip_h = true
	elif Input.is_action_just_pressed("jump") and index != 0:
		if index == 1:
			lobby = load("res://Scenes/Stages/Forest/Lobby.tscn")
			initiate_lobby()

	if rob.unit_offset == 1.0 and active:
		rob.get_parent().remove_child(rob)
		add_child(rob)
		active = false
	
	if active:
		rob.get_node("AnimatedSprite").play("walk")
		rob.set_offset(rob.get_offset() + 250 * delta)
	else: 
		rob.get_node("AnimatedSprite").play("idle")

func initiate_lobby():
	if lobby != null:
		var lobby_instance = lobby.instance()
		var fader = get_parent().get_node("SceneTranstition/Fader")
		var tween = get_parent().get_node("SceneTranstition/Tween")
		tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 1, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tween.start()
		yield(tween, "tween_completed")
		get_parent().add_child(lobby_instance)
		get_parent().remove_child(self)
		tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN)
		tween.start()
		yield(tween, "tween_completed")
		queue_free()
