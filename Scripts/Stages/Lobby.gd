extends Node2D

export var world_name = ""
var stage_name
var index = 0
var active = false
onready var rob = get_node("Rob")

var rob_lives = 5
var rob_marbles = 0
var rob_checkpoint
var rob_stages_completed = 0

func _process(delta):
	if Input.is_action_just_pressed("right") and index < 1 and !active:
		remove_child(rob)
		rob.set_offset(0)
		get_node("Path0To1").add_child(rob)
		active = true
		index += 1
		rob.get_node("AnimatedSprite").flip_h = false
	elif Input.is_action_just_pressed("left") and index > 0 and !active:
		remove_child(rob)
		rob.set_offset(0)
		get_node("Path1To0").add_child(rob)
		active = true
		index -= 1
		rob.get_node("AnimatedSprite").flip_h = true
	elif Input.is_action_just_pressed("jump") and index != 0:
		if index == 1:
			stage_name = world_name + "Stage" + str(index) + ".tscn"
			var stage = load("res://Scenes/Stages/" + world_name + "/" + stage_name)
			var new_stage = stage.instance()
			var game_node = get_parent()
			new_stage.get_node("Rob").instantiate(rob_lives, rob_marbles, rob_checkpoint)
			new_stage.get_node("Rob/OST/" + world_name).play()
			var fader = game_node.get_node("SceneTranstition/Fader")
			var tween = game_node.get_node("SceneTranstition/Tween")
			tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 1, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
			tween.start()
			yield(tween, "tween_completed")
			game_node.add_child(new_stage)
			if rob_checkpoint == stage_name.split(".")[0]:
				new_stage.get_node("Rob").global_position = new_stage.checkpoint_position
			else:
				new_stage.get_node("Rob").checkpoint = null
			game_node.remove_child(self)
			tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN)
			tween.start()
			yield(tween, "tween_completed")
			queue_free()

	if rob.unit_offset == 1.0 and active:
		rob.get_parent().remove_child(rob)
		add_child(rob)
		active = false
	
	if active:
		rob.get_node("AnimatedSprite").play("walk")
		rob.set_offset(rob.get_offset() + 250 * delta)
	else: 
		rob.get_node("AnimatedSprite").play("idle")

func instantiate(rob):
	rob_lives = rob.lives
	rob_marbles = rob.marbles
	rob_checkpoint = rob.checkpoint
	rob_stages_completed = rob.stages_completed
