extends Area2D

export var stage = ""
var scene
var world
var state = false

func _ready():
	scene = load("res://Scenes/Stages/" + stage)
	world = stage.split("/")[0]

func _on_MapStarter_body_entered(body):
	if body.get_name() == "Rob":
		body.interact_object = self

func _on_MapStarter_body_exited(body):
	if body.get_name() == "Rob":
		body.interact_object = null

func activate():
	if state:
		var new_stage = scene.instance()
		var game_node = get_parent().get_parent()
		new_stage.get_node("Rob").instantiate(get_parent().get_node("Rob"))
		new_stage.get_node("Rob/OST/" + world).play()
		var fader = game_node.get_node("SceneTranstition/Fader")
		var tween = game_node.get_node("SceneTranstition/Tween")
		tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 1, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
		tween.start()
		yield(tween, "tween_completed")
		game_node.add_child(new_stage)
		if get_parent().get_node("Rob").checkpoint == stage.split("/")[1].split(".")[0]:
			new_stage.get_node("Rob").global_position = new_stage.checkpoint_position
		else:
			new_stage.get_node("Rob").checkpoint = null
		game_node.remove_child(get_parent())
		game_node.remove_child(self)
		tween.interpolate_property(fader, "modulate:a", fader.modulate.a, 0, 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN)
		tween.start()
		yield(tween, "tween_completed")
		get_parent().queue_free()
		
