extends Control

var minutes = 3
var seconds = 0
var timerRunning = true
var t = null
var filledHeart = load("res://Assets/HUD/heartfilled.png")
var emptyHeart = load("res://Assets/HUD/heartempty.png")
var stage_size = 0
var progress
# Called when the node enters the scene tree for the first time.
func _ready():
	progress = get_node("ProgressBar/Path2D/PathFollow2D")
	var used_cells = get_parent().get_parent().get_node("Ground").get_used_cells()
	for pos in used_cells:
		if pos.x > stage_size:
			stage_size = int(pos.x)
	adjustHUD(get_parent().get_parent().get_node("Rob"))
	t = Timer.new()
	add_child(t)
	t.connect("timeout", self, "runTimer")
	t.set_wait_time(1.0)
	t.set_one_shot(false)
	t.start()

func adjustHUD(rob):
	get_node("Lives").text = str(rob.lives).pad_zeros(2)
	get_node("MarblesCount").text = str(rob.marbles).pad_zeros(2)
	
	get_node("Heart1").set_texture(filledHeart)
	get_node("Heart2").set_texture(filledHeart)
	get_node("Heart3").set_texture(filledHeart)
	
	if rob.health == 2:
		get_node("Heart3").set_texture(emptyHeart)
	elif rob.health == 1:
		get_node("Heart2").set_texture(emptyHeart)
		get_node("Heart3").set_texture(emptyHeart)
	elif rob.health == 0:
		get_node("Heart1").set_texture(emptyHeart)
		get_node("Heart2").set_texture(emptyHeart)
		get_node("Heart3").set_texture(emptyHeart)

func runTimer():
	if timerRunning:
		if seconds == 0:
			minutes -= 1
			seconds = 59
		else:
			seconds -= 1
		
		get_node("TimeValue").text = str(minutes).pad_zeros(2) + ":" + str(seconds).pad_zeros(2)
		
		if minutes == 0 and seconds <= 15:
			if !get_parent().get_parent().get_node("Rob").get_node("Sound/TimerEnding").playing:
				get_parent().get_parent().get_node("Rob").get_node("Sound/TimerEnding").play()
		
		if minutes == 0 and seconds == 0:
			timerRunning = false
			get_parent().get_parent().get_node("Rob").die()

func atualizeProgressBar(rob):
	progress.set_unit_offset(rob.global_position.x/4 / (stage_size * 16))
