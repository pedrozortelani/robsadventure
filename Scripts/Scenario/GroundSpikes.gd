extends Node2D

enum STATE{
	Stop,
	Attack,
	Retract
}

var cur_state = STATE.Stop
var next_state = STATE.Stop
var target = null
var timerOn = false

func _physics_process(delta):
	match cur_state:
		STATE.Stop:
			if $AnimatedSprite.animation != "Stop":
				$AnimatedSprite.play("Stop")
			if not timerOn:
				timerOn = true
				var t = Timer.new()
				t.set_wait_time(3)
				t.set_one_shot(true)
				self.add_child(t)
				t.start()
				yield(t,"timeout")
				next_state = STATE.Attack
				timerOn = false
		STATE.Attack:
			if $AnimatedSprite.animation != "Attack":
				$AnimatedSprite.play("Attack")
			if target:
				target.take_damage(1)
			if not timerOn:
				timerOn = true
				var t = Timer.new()
				t.set_wait_time(4)
				t.set_one_shot(true)
				self.add_child(t)
				t.start()
				yield(t,"timeout")
				next_state = STATE.Retract
				timerOn = false
		STATE.Retract:
			if $AnimatedSprite.animation != "Retract":
				$AnimatedSprite.play("Retract")
			if not timerOn:
				timerOn = true
				var t = Timer.new()
				t.set_wait_time(2)
				t.set_one_shot(true)
				self.add_child(t)
				t.start()
				yield(t,"timeout")
				next_state = STATE.Stop
				timerOn = false
	
	if cur_state != next_state:
		cur_state = next_state

func _on_DamageArea_body_entered(body):
	if body.get_name() == "Rob":
		target = body


func _on_DamageArea_body_exited(body):
	if body.get_name() == "Rob":
		target = null
