extends Area2D

func _on_Area2D_body_entered(body):
	if body.get_name() == "Rob":
		body.checkpoint = null
		body.celebrate()
		var stage = get_parent().get_parent()
		body.stages_completed[stage.world_index] = stage.number
		get_parent().get_node("AnimatedSprite").play("End")
