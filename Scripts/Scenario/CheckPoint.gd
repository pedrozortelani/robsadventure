extends Area2D

var isOn = false

func _on_CheckPoint_body_entered(body):
	if body.get_name() == "Rob" and !isOn:
		isOn = true
		$AnimatedSprite.play("activate")
		body.checkpoint = get_parent().get_name()
		$Audio.play()


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "activate":
		$AnimatedSprite.play("on")
