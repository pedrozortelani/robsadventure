extends Area2D

var Rob = null
var opened = false
export var powerup_name = ""
export var instantiable = false
const powerup = preload("res://Scenes/PowerUps/PowerUp.tscn")
const open_texture = preload("res://Assets/Scenario/ToyChestOpened.png")

func _on_PowerUpChest_body_entered(body):
	if body.get_name() == "Rob":
		Rob = body
		body.interact_object = self

func _on_PowerUpChest_body_exited(body):
	if body.get_name() == "Rob":
		body.interact_object = null
		Rob = null

func activate():
	if not opened:
		$Open.play()
		opened = true
		var new_powerup = powerup.instance()
		new_powerup.instantiable = instantiable
		new_powerup.powerup = powerup_name		
		add_child(new_powerup)
		new_powerup.set_position(Vector2(0, -20))
		new_powerup.get_node("AnimatedSprite").play(powerup_name)
		$Sprite.set_texture(open_texture)
