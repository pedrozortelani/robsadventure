extends KinematicBody2D

var active = false
var rotating = true
var speed = 400 
var velocity = Vector2(0, 0)
var dir 

func _ready():
	dir = get_scale().x

func _physics_process(delta):
	if active:
		if rotating:
			velocity.x = speed * dir
			if is_on_floor():
				velocity.y = 0;
				$Sprite.rotation_degrees += 10
			else:
				velocity.y = 500
				$Sprite.rotation_degrees += 5
		else:
			 velocity.x = 0
		
		var last_position = get_position().x
		
		move_and_slide(velocity, Vector2.UP) 
		var slide_count = get_slide_count()
		if slide_count:
			if get_slide_collision(slide_count - 1).collider.get_name() == "Rob":
				get_slide_collision(slide_count - 1).collider.take_damage(3)
		if get_position().x - last_position < 0.8 :
			rotating = false
			
		if get_position().y > 500:
			queue_free()

func _on_Trigger_body_exited(body):
	if body.get_name() == "Rob":
		if body.get_position().x * get_scale().x > get_position().x :
			active = true
