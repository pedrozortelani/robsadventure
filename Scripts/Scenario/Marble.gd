extends Area2D

var pickedup = false

func _on_Marble_body_entered(body):
	if body.get_name() == "Rob" and not pickedup:
		body.addMarble()
		$Pickup.play()
		pickedup = true
		$AnimatedSprite.play("Pickup")


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "Pickup":
		queue_free()
