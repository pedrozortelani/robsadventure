extends StaticBody2D

var shooting = false
var alive = true
var health = 3
var cannonBall = preload("res://Scenes/Enemies/CannonBall.tscn")

func _process(delta):
	if !shooting and alive:
		shooting = true
		var t = Timer.new()
		t.set_wait_time(3)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t,"timeout")
		shoot()

func shoot():
	if alive:
		$AnimatedSprite.play("Attack")
		
		var t = Timer.new()
		t.set_wait_time(0.43)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t,"timeout")
		
		$Sounds/Attack.play()
		var new_cannonBall = cannonBall.instance()
		new_cannonBall.dir = get_scale().x
		new_cannonBall.position = Vector2(24, 4)
		add_child(new_cannonBall)
		shooting = false
		
func take_damage(dmg):
	$AnimatedSprite.play("Hit")
	if alive:
		health -= 1
		if health == 0:
			alive = false
			get_node("CollisionShape2D").set_deferred("disabled", true)
			$Sounds/Death.play()
			$AnimatedSprite.play("Die")
		else:
			$Sounds/Hit.play()

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "Attack" or $AnimatedSprite.animation == "Hit":
		$AnimatedSprite.play("Idle")

func _on_BounceArea_body_entered(body):
	if body.get_name() == "Rob" and alive:
		body.velocity.y = body.jump_force
		self.take_damage(1)
