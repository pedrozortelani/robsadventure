extends KinematicBody2D

var timer
var cur_state = STATE.Borning
var nxt_state = STATE.Borning
var health = 2
var enemy = null
var target_position
var velocity = Vector2.ZERO
var speed = 400
var acting = false
var time_count = false
var alive = true
var direction_modifier = -0.25
var old_position
var x_variation
var going_down = true
var sting_scene = preload("res://Scenes/Stages/Forest/Sting.tscn")

enum STATE{
	Borning,
	Idle,
	Walk,
	Die,
	Shoot
}

func _ready():
	nxt_state = STATE.Borning

func _process(delta):
	
	if is_instance_valid(enemy):
		if global_position.x > enemy.global_position.x:
			$AnimatedSprite.flip_h = false
		else:
			$AnimatedSprite.flip_h = true
	
	match cur_state:
		STATE.Borning:
			velocity = Vector2.ZERO
			$AnimatedSprite.play("borning")
			
		STATE.Idle:
			if !$Sounds/Idle.playing:
				$Sounds/Idle.play()
				
			velocity = Vector2.ZERO
			if $AnimatedSprite.animation != "hit":
				$AnimatedSprite.play("idle")
			
			if !acting:
				if is_instance_valid(enemy):
					acting = true
					var t = Timer.new()
					t.set_wait_time(1)
					t.set_one_shot(true)
					self.add_child(t)
					t.start()
					yield(t,"timeout")
					
					if nxt_state != STATE.Die:
						nxt_state = STATE.Shoot
						$Sounds/Idle.stop()
					
					acting = false
				else:
					acting = true
					var t = Timer.new()
					t.set_wait_time(2)
					t.set_one_shot(true)
					self.add_child(t)
					t.start()
					yield(t,"timeout")
					
					if nxt_state != STATE.Die:
						nxt_state = STATE.Walk
						time_count = false
						$Sounds/Idle.stop()
					
					acting = false
				
		STATE.Walk:
			$AnimatedSprite.play("walk")
			if !$Sounds/Walk.playing:
				$Sounds/Walk.play()
				
			if !acting:
				acting = true
				target_position = get_random_position(100)
			
				if target_position.x < global_position.x:
					$AnimatedSprite.flip_h = false
				else:
					$AnimatedSprite.flip_h = true

			if acting:
				if !time_count:
					time_count = true
					var t1 = Timer.new()
					t1.connect("timeout", self, "cancel_attack")
					t1.set_wait_time(3)
					t1.set_one_shot(true)
					self.add_child(t1)
					t1.start()
					
				if abs(global_position.y - target_position.y) < 1 and abs(global_position.x - target_position.x) < 1:
					if is_instance_valid(enemy) and nxt_state != STATE.Die:
						nxt_state = STATE.Shoot
						acting = false
						$Sounds/Walk.stop()
					elif nxt_state != STATE.Die:
						nxt_state = STATE.Idle
						acting = false
						$Sounds/Walk.stop()
				
			velocity = (target_position - global_position).normalized() * speed
				
			
			
		STATE.Die:
			velocity = Vector2.ZERO
			alive = false
			get_node("CollisionShape2D").set_deferred("disabled", true)
			$AnimatedSprite.play("die")
			
		STATE.Shoot:
			velocity = Vector2.ZERO
			
			if is_instance_valid(enemy):
				if !acting:
					acting = true
					var enemy_position = enemy.global_position
					$AnimatedSprite.play("shoot")
					
					var t = Timer.new()
					t.set_wait_time(0.6)
					t.set_one_shot(true)
					self.add_child(t)
					t.start()
					yield(t,"timeout")
					
					$Sounds/Shoot.play()
					var new_sting = sting_scene.instance()
					if is_instance_valid(enemy):
						new_sting.target = enemy.global_position
					else:
						new_sting.target = enemy_position
					if $AnimatedSprite.flip_h:
						new_sting.position = Vector2(4, 8)
					else:
						new_sting.position = Vector2(-4, 8)
					add_child(new_sting)
	
			if $AnimatedSprite.animation == "idle":
				nxt_state = STATE.Idle
				acting = false
				
	var collision = move_and_collide(velocity * delta)
	if collision:
		if collision.collider.get_name() == "Rob" and alive:
			collision.collider.take_damage(1)
	
	if nxt_state != cur_state:
		cur_state = nxt_state

func take_damage(dmg):
	health -= dmg
	
	if health == 0:
		$Sounds/Death.play()
		get_parent().wasp_count -= 1
		nxt_state = STATE.Die
	else:
		$Sounds/Hit.play()
		$AnimatedSprite.play("hit")

func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "borning":
		nxt_state = STATE.Idle
	elif $AnimatedSprite.animation == "die":
		queue_free()
	elif $AnimatedSprite.animation == "shoot" and nxt_state != STATE.Die:
		$AnimatedSprite.play("idle")
	elif $AnimatedSprite.animation == "hit":
		$AnimatedSprite.play("idle")

func _on_Detection_body_entered(body):
	if body.get_name() == "Rob":
		enemy = body

func _on_Detection_body_exited(body):
	if body.get_name() == "Rob":
		enemy = null
		nxt_state = STATE.Idle
		acting = false

func _on_BounceArea_body_entered(body):
	if body.get_name() == "Rob" and alive:
		body.velocity.y = body.jump_force
		self.take_damage(1)

func cancel_attack():
	if cur_state == STATE.Walk:
		nxt_state = STATE.Idle
		acting = false
		$Sounds/Walk.stop()

func get_random_position(radius):
	var step = 2 * PI / 100
	var random_num = rand_range(0, 100)
	var random_position = global_position + Vector2(radius, 0).rotated(step * random_num)
	return random_position
