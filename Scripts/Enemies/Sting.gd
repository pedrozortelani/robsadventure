extends KinematicBody2D


var velocity = Vector2.ZERO
var speed = 600
var target

func _ready():
	velocity = (target - global_position).normalized()
	rotation = velocity.angle()

func _process(delta):
	var collision = move_and_collide(velocity * speed * delta)
	if collision:
		if collision.collider.get_name() == "Rob":
			collision.collider.take_damage(1)
		queue_free()
