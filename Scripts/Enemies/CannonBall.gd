extends KinematicBody2D

var speed = 550
var velocity = Vector2.ZERO
var dir
var initial_position

func _ready():
	velocity = Vector2(dir, 0)
	initial_position = get_position().x

func _process(delta):
	var collision = move_and_collide(velocity * speed * delta)
	
	if collision:
		if collision.collider.get_name() == "Rob":
			collision.collider.take_damage(1)
			$AnimatedSprite.play("out")
			velocity = Vector2.ZERO
		else:
			$AnimatedSprite.play("out")
			velocity = Vector2.ZERO
			
	if abs(initial_position - get_position().x) > 400:
		$AnimatedSprite.play("out")
		velocity = Vector2.ZERO


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "out":
		queue_free()
