extends KinematicBody2D

var enemy = null
var target = null
var cur_dir = 1
var walking = true
var attacking = false
var alive = true
var destiny = 100
var is_right = true
var health = 3
var velocity = Vector2(0, 500)

func _physics_process(delta):
	if alive:
		if walking:
			if $Content/AnimatedSprite.animation != "Hit":
				$Content/AnimatedSprite.play("Walk")
			if enemy:
				if enemy.global_position.x < global_position.x:
					$Content.set_scale(Vector2(-1,1))
					if get_node("CollisionShape2D").get_position().x < 0:
						var cld_position = get_node("CollisionShape2D").get_position()
						get_node("CollisionShape2D").set_position(Vector2(cld_position.x * -1, cld_position.y))
				else:
					$Content.set_scale(Vector2(1,1))
					if get_node("CollisionShape2D").get_position().x > 0:
						var cld_position = get_node("CollisionShape2D").get_position()
						get_node("CollisionShape2D").set_position(Vector2(cld_position.x * -1, cld_position.y))
				var dir = (Vector2(enemy.global_position.x, global_position.y)  - Vector2(global_position.x, global_position.y)).normalized()
				velocity.x = dir.x * 200
				
				move_and_slide(velocity)
			else:
				var dir
				if is_right:
					$Content.set_scale(Vector2(1,1))
					if get_node("CollisionShape2D").get_position().x > 0:
						var cld_position = get_node("CollisionShape2D").get_position()
						get_node("CollisionShape2D").set_position(Vector2(cld_position.x * -1, cld_position.y))
					dir = (Vector2(global_position.x, global_position.y) - Vector2(global_position.x - destiny, global_position.y)).normalized()
				elif not is_right:
					$Content.set_scale(Vector2(-1,1))
					if get_node("CollisionShape2D").get_position().x < 0:
						var cld_position = get_node("CollisionShape2D").get_position()
						get_node("CollisionShape2D").set_position(Vector2(cld_position.x * -1, cld_position.y))
					dir = (Vector2(global_position.x, global_position.y) - Vector2(global_position.x + destiny, global_position.y)).normalized()
				
				var collision = move_and_collide(dir * 200 * delta)
				if collision:
					destiny = 0
				
				if destiny == 0:
					walking = false
					$Content/AnimatedSprite.play("Idle")
					var t = Timer.new()
					t.set_wait_time(3)
					t.set_one_shot(true)
					self.add_child(t)
					t.start()
					yield(t,"timeout")
					is_right = !is_right
					destiny = 100
					walking = true
					
				destiny -= 1
		elif not attacking and target:
			attacking = true
			$Content/AnimatedSprite.play("Attack")
			var t = Timer.new()
			t.set_wait_time(0.5)
			t.set_one_shot(true)
			self.add_child(t)
			t.start()
			yield(t,"timeout")
			$Sounds/Attack.play()
			if alive:
				if target:
					target.take_damage(1)
					
				$Content/AnimatedSprite.play("Tired")
				var t2 = Timer.new()
				t2.set_wait_time(3)
				t2.set_one_shot(true)
				self.add_child(t2)
				t2.start()
				yield(t2,"timeout")
				$Content/AnimatedSprite.play("Idle")
				attacking = false
				if !target:
					walking = true
	
	
func _on_Detection_body_entered(body):
	if body.get_name() == "Rob":
		enemy = body

func _on_Detection_body_exited(body):
	if body.get_name() == "Rob":
		enemy = null


func _on_Attack_body_entered(body):
	if alive:
		if body.get_name() == "Rob":
			target = body
			if not attacking:
				attacking = true
				walking = false
				$Content/AnimatedSprite.play("Attack")
				var t = Timer.new()
				t.set_wait_time(0.5)
				t.set_one_shot(true)
				self.add_child(t)
				t.start()
				yield(t,"timeout")
				$Sounds/Attack.play()
				if target:
					target.take_damage(1)
					
				$Content/AnimatedSprite.play("Tired")
				var t2 = Timer.new()
				t2.set_wait_time(2.5)
				t2.set_one_shot(true)
				self.add_child(t2)
				t2.start()
				yield(t2,"timeout")
				if alive:
					$Content/AnimatedSprite.play("Idle")
					attacking = false
					if !target:
						walking = true

func _on_Attack_body_exited(body):
	if body.get_name() == "Rob":
		target = null
		
func take_damage(dmg):
	$Content/AnimatedSprite.play("Hit")
	if alive:
		health -= dmg
		if health <= 0:
			alive = false
			get_node("CollisionShape2D").set_deferred("disabled", true)
			$Sounds/Death.play()
			$Content/AnimatedSprite.play("Die")
		else:
			$Sounds/Hit.play()

func _on_AnimatedSprite_animation_finished():
	if $Content/AnimatedSprite.animation == "Die":
		var t = Timer.new()
		t.set_wait_time(3)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t,"timeout")
		queue_free()
	elif $Content/AnimatedSprite.animation == "Hit":
		if velocity.x != 0:
			$Content/AnimatedSprite.play("Walk")
		else:
			$Content/AnimatedSprite.play("Idle")

func _on_BounceArea_body_entered(body):
	if body.get_name() == "Rob" and alive:
		body.velocity.y = body.jump_force
		self.take_damage(1)
