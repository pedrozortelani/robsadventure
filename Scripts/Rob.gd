extends KinematicBody2D

enum STATE{
	Idle,
	Run,
	Jump,
	Celebrate
}

var HUD
var checkpoint = null

var health = 3
var lives = 5
var targetable = true
var marbles = 0

var powerup = ""
var interact_object = null

const baseball = preload("res://Scenes/PowerUps/Baseball.tscn")

var stages_completed = [0]

var velocity = Vector2.ZERO
var speed_modifier = 1
var movable = true
var jump_force = -850
var fall_force = 2000
var cur_dir = 1
var next_dir = 1
var cur_state = STATE.Idle
var next_state = STATE.Idle
var can_jump = true
var onVine = 0
var powerup_count = 0
var walk_playback

func _ready():
	HUD = get_parent().get_node("CanvasLayer/UI")

func _physics_process(delta):
	
	velocity.x = 0
	
	if Input.is_action_just_pressed("action"):
		if is_on_floor():
			speed_modifier = 1.2
		if interact_object:
			interact_object.activate()
		elif powerup:
			use_powerup()
			
	if Input.is_action_just_released("action"):
		speed_modifier = 1
		
	if Input.is_action_pressed("right"):
		velocity.x = 400 * speed_modifier
		next_dir = 1
	elif Input.is_action_pressed("left"):
		velocity.x = -400 * speed_modifier
		next_dir = -1
	
	match cur_state:
		STATE.Idle:
			if $AnimatedSprite.animation != "Hit" and $AnimatedSprite.animation != "Throw" and $AnimatedSprite.animation != "Die" and $AnimatedSprite.animation != "Celebrate":
				$AnimatedSprite.play("Idle")
			if velocity.x != 0:
				next_state = STATE.Run
				
			if is_on_floor() && Input.is_action_just_pressed("jump"):
				$Sound/Jump.play()
				velocity.y = jump_force
				next_state = STATE.Jump
		STATE.Run:
			if $AnimatedSprite.animation != "Hit" and $AnimatedSprite.animation != "Throw" and $AnimatedSprite.animation != "Die" and $AnimatedSprite.animation != "Celebrate":
				$AnimatedSprite.play("Run")
				if !$Sound/Run.playing:
					$Sound/Run.play()
				$Sound/Run.stream_paused = false
					
			if is_on_floor() and Input.is_action_just_pressed("jump"):
				$Sound/Run.stream_paused = true
				velocity.y = jump_force
				next_state = STATE.Jump
				$Sound/Jump.play()
				can_jump = false
			if velocity.x == 0:
				next_state = STATE.Idle
				$Sound/Run.stream_paused = true
			if velocity.y > 0:
				next_state = STATE.Jump
				$Sound/Run.stream_paused = true
		STATE.Jump:
			if $AnimatedSprite.animation != "Hit" and $AnimatedSprite.animation != "Throw" and $AnimatedSprite.animation != "Die" and $AnimatedSprite.animation != "Celebrate":
				if velocity.y < 0:
					$AnimatedSprite.play("Jump")
				if velocity.y > 0 and $AnimatedSprite.animation != "WallSlide":
					$AnimatedSprite.play("Fall")
			if can_jump and Input.is_action_just_pressed("jump"):
				$Sound/Jump.play()
				fall_force = 2000
				velocity.y = jump_force
				can_jump = false
			if is_on_floor():
				if velocity.x == 0:
					next_state = STATE.Idle
				else:
					next_state = STATE.Run
		STATE.Celebrate:
			velocity = Vector2.ZERO
			$AnimatedSprite.play("Celebrate")
		
	if velocity.y < 600:
		velocity.y += fall_force * delta
		
	if movable:
		velocity = move_and_slide(velocity, Vector2.UP)
		if is_instance_valid(HUD):
			HUD.atualizeProgressBar(self)
	
	var slide_count = get_slide_count()
	if slide_count:
		if is_instance_valid(get_slide_collision(slide_count - 1).collider):
			if get_slide_collision(slide_count - 1).collider.get_collision_layer() == 2:
				if get_slide_collision(slide_count - 1).collider.get("alive"):
					if get_slide_collision(slide_count - 1).collider.alive:
						take_damage(1)
				else:
					take_damage(1)
	
	if cur_dir != next_dir and movable:
		cur_dir = next_dir
		$AnimatedSprite.flip_h = !$AnimatedSprite.flip_h
		
	if cur_state != next_state:
		cur_state = next_state

func addMarble():
	marbles += 1
	if marbles == 50:
		$Sound/OneLiveUp.play()
		lives += 1
		marbles = 0
	HUD.adjustHUD(self)

func use_powerup():
	match powerup:
		"Baseball":
			if powerup_count < 3:
				$Sound/Throw.play()
				powerup_count += 1
				var baseball_instance = baseball.instance()
				baseball_instance.dir = cur_dir
				get_parent().add_child(baseball_instance)
				baseball_instance.set_position(get_position())
				baseball_instance.rob = self
				$AnimatedSprite.play("Throw")
		"PalmLeaf":
			pass
			
func take_damage(dmg):
	if targetable:
		targetable = false
		if powerup:
			if is_instance_valid(get_node(powerup)):
				if powerup == "PalmLeaf":
					fall_force = 2000
				get_node(powerup).queue_free()
			powerup = null
		health -= dmg
		
		if health > 0:
			$AnimatedSprite.play("Hit")
			$Sound/Hit.play()
		elif health <= 0:
			die()
		
		HUD.adjustHUD(self)
		
		var t = Timer.new()
		t.set_wait_time(3)
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t,"timeout")
		targetable = true


func _on_AnimatedSprite_animation_finished():
	if $AnimatedSprite.animation == "Hit":
		$AnimatedSprite.play("Idle")
	elif $AnimatedSprite.animation == "Throw":
		$AnimatedSprite.play("Idle")

func die():
	$Sound/Death.play()
	lives -= 1
	movable = false
	$AnimatedSprite.play("Die")

func celebrate():
	movable = false
	$Sound/Celebrate.play()
	$AnimatedSprite.play("Celebrate")

func instantiate(lives, marbles, checkpoint):
	self.lives = lives
	self.marbles = marbles
	self.checkpoint = checkpoint

func _on_Death_finished():
	get_parent().end_stage()
	movable = true


func _on_Celebrate_finished():
	get_parent().end_stage()
	movable = true
